package eu.jpereira.exercises.application;

import eu.jpereira.exercises.bets.BetCollector;
import eu.jpereira.exercises.bets.ConsoleBetCollector;
import eu.jpereira.exercises.bets.CurrentBetsBook;
import eu.jpereira.exercises.bets.InMemoryCurrentBetsBook;
import eu.jpereira.exercises.coupier.Coupier;
import eu.jpereira.exercises.coupier.DefaultCoupier;
import eu.jpereira.exercises.coupier.DefaultResultsCalculator;
import eu.jpereira.exercises.coupier.ResultsCalculator;
import eu.jpereira.exercises.roulette.*;
import eu.jpereira.exercises.scores.*;

import java.io.File;

/**
 * Main Class for the Console RouletteApplication
 */
public class ConsoleRouletteApplication {


    private static final int LOWER_BOUND_RANDOM_ALGORITHM = 0;
    private static final int UPPER_BOUND_RANDOM_ALGORITHM = 36;
    private static final int INTERVAL_SECONDS_TO_SPIN_ROULETTE = 30;

    /**
     * Requires one argument that is the path to a file with the player's name's and past history
     *
     * @param args
     */
    public static void main(String[] args) throws ScoreBookFileParserException {


        File file = extractFilePathOrFail(args);

        //I chose not to use a dependency injection here, however I compose all the application in the following lines of code

        //Assemble the algorithm that produces a random number
        RouletteAlgorithm rouletteAlgorithm = new SimpleRandomAlgorithm(LOWER_BOUND_RANDOM_ALGORITHM, UPPER_BOUND_RANDOM_ALGORITHM);
        //Create a roulette and let it use the algorithm
        Roulette roulette = new DefaultRoulette(rouletteAlgorithm);


        //A Scheduled ScheduledRouletteSpinner is programmed to spin a roulette at a given rate
        //To build a ScheduledRouletteSpinner we need to provide the interval and the roulette it will spin
        ScheduledRouletteSpinnerConfig scheduledRouletteSpinnerConfig = new ScheduledRouletteSpinnerConfig.Builder().
                withRoulette(roulette).
                withFixedIntervalSeconds(INTERVAL_SECONDS_TO_SPIN_ROULETTE).build();

        //We have an abstraction for a spinner
        RouletteSpinner rouletteSpinner = new ScheduledRouletteSpinner(scheduledRouletteSpinnerConfig);

        //Hold the current bets
        CurrentBetsBook currentBetBook = new InMemoryCurrentBetsBook();

        //Parse the file provide to read history from. WILL NOT WRITE ANY HISTORY TO THE FILE
        ScoreBookFileParser fileParser = new DefaultScoreBookFileParser(file);
        //A ScoreBook keeps historical scores
        ScoreBoard scoreBook = new InMemoryScoreBoard(fileParser.parse());

        ResultsCalculator resultsCalculator = new DefaultResultsCalculator();
        //The coupier is the main actor and it controls the currentBetBook, scoreBook, roulette and rouletteSpinner
        Coupier coupier = new DefaultCoupier(currentBetBook, scoreBook, roulette, rouletteSpinner, resultsCalculator);


        coupier.startAcceptingBets();

        //Collects bets
        BetCollector betCollector = new ConsoleBetCollector(coupier);
        betCollector.starCollectingBets();
        coupier.stopAcceptingBets();

    }

    /**
     * Simple utility method to verify is the arguments contain the file name and return it.
     * It prints a error message and exit if no arguments are provided or file does not exists or is a directory
     *
     * @param args
     * @return
     */
    private static File extractFilePathOrFail(String[] args) {

        if (args.length == 0) {
            printHelpMessageAndAbortApplication();
        }
        File file = new File(args[0]);

        if (!file.exists() || file.isDirectory()) {
            printHelpMessageAndAbortApplication();

        }
        return file;
    }

    private static void printHelpMessageAndAbortApplication() {
        System.out.println("Provide in the first argument a full path for a valid player's data file");
        System.exit(0);
    }


}
