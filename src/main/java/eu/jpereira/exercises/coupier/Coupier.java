package eu.jpereira.exercises.coupier;

import eu.jpereira.exercises.bets.Bet;

/**
 * The coupier is acting as a mediator between all components of the application
 */
public interface Coupier {

    /**
     * Start accepting bets. It gives a chance to the coupier to set up all its collaborators
     */
    void startAcceptingBets();

    /**
     * Accept a bet. If the copier is busy caller threads will wait until copier is ready again
     * @param bet The bet to place
     * @throws BetNotAcceptedException
     */
    void acceptBet(Bet bet) throws BetNotAcceptedException;

    /**
     * Stop accepting bets.
     * It gives a chance to coupier to tear down its collabortots
     */
    void stopAcceptingBets();
}
