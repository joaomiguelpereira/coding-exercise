package eu.jpereira.exercises.coupier;

/**
 * A coupier couldn't accept a bet
 */
public class BetNotAcceptedException extends Exception {
    public BetNotAcceptedException(String message) {
        super(message);
    }
}
