package eu.jpereira.exercises.coupier;

import eu.jpereira.exercises.bets.Bet;
import eu.jpereira.exercises.bets.BetResults;
import eu.jpereira.exercises.bets.CurrentBetsBook;
import eu.jpereira.exercises.roulette.Roulette;
import eu.jpereira.exercises.roulette.RouletteObserver;
import eu.jpereira.exercises.roulette.RouletteSpinner;
import eu.jpereira.exercises.scores.ScoreBoard;

import java.util.concurrent.locks.ReentrantLock;

/**
 * A default implementation of a Coupier
 * It acts as a mediator between all the colaborators
 * This coupier is also an observer or roulette spin results
 */
public class DefaultCoupier implements Coupier, RouletteObserver {


    //Hold the current bets
    private final CurrentBetsBook currentBetBook;
    //The score book
    private final ScoreBoard scoreBook;
    //The roulette spinner
    private final RouletteSpinner rouletteSpinner;
    //The roulette
    private final Roulette roulette;
    //The results calculator
    private final ResultsCalculator resultsCalculator;
    //Use this lock to lock threads in the both places where multiple threads may access
    private final ReentrantLock lock = new ReentrantLock();

    public DefaultCoupier(CurrentBetsBook theCurrentBetBook, ScoreBoard theScoreBook, Roulette theRoulette, RouletteSpinner theRouletteSpinner, ResultsCalculator theResultsCalculator) {
        assertArgumentsNotNull(theCurrentBetBook, theScoreBook,  theRoulette, theRouletteSpinner, theResultsCalculator);
        this.resultsCalculator = theResultsCalculator;
        this.currentBetBook = theCurrentBetBook;
        this.scoreBook = theScoreBook;
        this.rouletteSpinner = theRouletteSpinner;
        this.roulette = theRoulette;

    }



    @Override
    public void startAcceptingBets() {
        //Attach this as an observer of roullet results
        this.roulette.attachObserver(this);
        this.currentBetBook.reset();
        this.rouletteSpinner.start();
    }

    @Override
    public void acceptBet(Bet bet) throws BetNotAcceptedException {
        //must be for an existing player name
        if (!this.scoreBook.hasPlayer(bet.getPlayerName()) ) {
            throw new BetNotAcceptedException(String.format("The Player %s is not registered.", bet.getPlayerName()));
        }

        //While placing a bet it can't calculate any results
        try {
            lock.lock();
            this.currentBetBook.placeBet(bet);
        } finally {
            lock.unlock();
        }


    }

    @Override
    public void stopAcceptingBets() {
        this.rouletteSpinner.stop();
    }

    @Override
    public void resultChanged(int result) {
        System.out.println("Number: "+result);
        try {
            //Prevent any bets to be placed while calculating results
            lock.lock();

            BetResults results = resultsCalculator.calculate(this.currentBetBook.getBets(), result);
            //Print results
            System.out.println(results);
            this.scoreBook.update(results);
            //Print book score
            System.out.println(scoreBook);

            //Reset bets
            this.currentBetBook.reset();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Utility method to prevent null reference. Does not indicates which argument is null, though
     * @param arguments
     */
    private void assertArgumentsNotNull(Object... arguments) {
        for (Object object : arguments ) {
            if ( object == null ) {
                throw new IllegalArgumentException("Argument cannot be null");
            }
        }
    }
}
