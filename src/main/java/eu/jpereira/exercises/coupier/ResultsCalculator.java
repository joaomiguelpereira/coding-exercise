package eu.jpereira.exercises.coupier;

import eu.jpereira.exercises.bets.Bet;
import eu.jpereira.exercises.bets.BetOutcome;
import eu.jpereira.exercises.bets.BetResults;
import eu.jpereira.exercises.bets.CurrentBetsBook;

import java.util.List;

/**
 * Calculate results algorithm
 */
public interface ResultsCalculator {


    /**
     * For each bet create a new BetOutcome that will be composed by the bet itself and decorated with an outcome (WIN or LOOSE)
     * @param bets
     * @param result
     * @return
     */
    BetResults calculate(List<Bet> bets, int result);
}
