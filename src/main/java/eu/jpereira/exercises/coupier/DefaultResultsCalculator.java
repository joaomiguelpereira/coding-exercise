package eu.jpereira.exercises.coupier;

import eu.jpereira.exercises.bets.Bet;
import eu.jpereira.exercises.bets.BetOutcome;
import eu.jpereira.exercises.bets.BetResults;
import eu.jpereira.exercises.bets.BetType;

import java.math.BigDecimal;
import java.util.List;

/**
 * The algorithm to calculate the results of bets
 */
public class DefaultResultsCalculator implements ResultsCalculator {

    private static final String LOSE_OUTCOME = "LOSE";
    private static final String WIN_OUTCOME = "WIN";
    private static final String EVEN_REPRESENTATION = "EVEN";
    private static final String ODD_REPRESENTATION = "ODD";
    private static final int MULTIPLIER_NUMBER = 36;
    private static final int MULTIPLIER_EVEN_ODD = 2;

    @Override
    public BetResults calculate(List<Bet> bets, int result) {

        BetResults results = new BetResults();

        for (Bet bet : bets) {
            results.addOutcome(calculateOutcome(bet, result));
        }
        return results;

    }

    private BetOutcome calculateOutcome(Bet bet, int result) {

        //I the result is Zero, then always loose
        if (result == 0) {
            return new BetOutcome(bet, LOSE_OUTCOME, new BigDecimal("0.0"));
        }

        BetOutcome outcome;
        if (bet.getType().equals(BetType.NUMBER)) {
            if (result == bet.getBetGoalAsNumber()) {
                outcome = new BetOutcome(bet, WIN_OUTCOME, calculateWinningAmmount(MULTIPLIER_NUMBER, bet.getAmmount()));
            } else {
                outcome = new BetOutcome(bet, LOSE_OUTCOME, new BigDecimal("0.0"));
            }
        } else {
            boolean even = (result % 2) == 0;
            if ((even && bet.getBetGoal().equals(EVEN_REPRESENTATION)) || (!even && bet.getBetGoal().equals(ODD_REPRESENTATION))) {
                outcome = new BetOutcome(bet, WIN_OUTCOME, calculateWinningAmmount(MULTIPLIER_EVEN_ODD, bet.getAmmount()));
            } else {
                outcome = new BetOutcome(bet, LOSE_OUTCOME, new BigDecimal("0.0"));
            }
        }

        return outcome;
    }

    private BigDecimal calculateWinningAmmount(int multiplier, BigDecimal ammount) {
        return ammount.multiply(new BigDecimal(multiplier));
    }
}
