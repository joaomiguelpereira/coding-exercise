package eu.jpereira.exercises.bets;

/**
 * Collect bets and notify any observer of a new bet available
 */
public interface BetCollector {

    /**
     * Starts the function of collecting bets
     */
    void starCollectingBets();
}
