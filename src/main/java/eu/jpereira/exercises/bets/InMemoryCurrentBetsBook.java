package eu.jpereira.exercises.bets;

import eu.jpereira.exercises.view.TableRow;
import eu.jpereira.exercises.view.TabularView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This object keeps track of current open bets
 * It allows to have multiple bets per player
 */
public class InMemoryCurrentBetsBook implements CurrentBetsBook {

    private List<Bet> currentBets = new ArrayList();


    @Override
    public void placeBet(Bet bet) {
        if (bet == null) {
            throw new IllegalArgumentException("Bet cannot be null");
        }
        currentBets.add(bet);
    }

    @Override
    public List<Bet> getBets() {
        return Collections.unmodifiableList(currentBets);

    }


    @Override
    public void reset() {
        this.currentBets.clear();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        TabularView tabularView = new TabularView();
        tabularView.addHeaders("Player Name", "Bet", "Ammount");
        for (Bet bet : currentBets) {
            TableRow row = new TableRow();
            row.addCell(0, bet.getPlayerName());
            row.addCell(1, bet.getBetGoal());
            row.addCell(2, bet.getAmmount().toPlainString());
            tabularView.addRow(row);
        }
        sb.append(tabularView.toString());
        return sb.toString();


    }
}
