package eu.jpereira.exercises.bets;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 10:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class BetOutcome {


    private final Bet bet;
    private final String outcome;
    private final BigDecimal winAmmount;

    public BetOutcome(Bet theBet, String theOutcome, BigDecimal theWinAmmount) {
        this.bet = theBet;
        this.outcome = theOutcome;
        this.winAmmount = theWinAmmount;

    }

    public String getPlayerName() {
        return this.bet.getPlayerName();
    }

    public String getBetGoal() {
        return this.bet.getBetGoal();
    }

    public String getOutcome() {
        return outcome;
    }

    public BigDecimal getwinAmmount() {
        return this.winAmmount;

    }

    public BigDecimal getBetAmmount() {
        return this.bet.getAmmount();
    }
}
