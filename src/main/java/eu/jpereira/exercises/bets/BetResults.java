package eu.jpereira.exercises.bets;

import eu.jpereira.exercises.view.TableRow;
import eu.jpereira.exercises.view.TabularView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 10:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class BetResults {

    private List<BetOutcome> outcomes = new ArrayList<BetOutcome>();

    public void addOutcome(BetOutcome betOutcome) {

        this.outcomes.add(betOutcome);
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        TabularView tabularView = new TabularView();
        tabularView.addHeaders("Player", "Bet", "Outcome", "Winnings");
        for (BetOutcome betOutcome : outcomes) {
            TableRow row = new TableRow();
            row.addCell(0, betOutcome.getPlayerName());
            row.addCell(1, betOutcome.getBetGoal());
            row.addCell(2, betOutcome.getOutcome());
            row.addCell(3, betOutcome.getwinAmmount().toPlainString());
            tabularView.addRow(row);
        }
        sb.append(tabularView.toString());
        return sb.toString();
    }

    public List<BetOutcome> getBetOutComes() {
        return Collections.unmodifiableList(this.outcomes);
    }
}
