package eu.jpereira.exercises.bets;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/15/14
 * Time: 10:55 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CurrentBetsBook {

    void placeBet(Bet bet);

    /**
     * Get a read only list of current bets
     * @return
     */
    List<Bet> getBets();
    /**
     * Reset any state
     */
    void reset();
}
