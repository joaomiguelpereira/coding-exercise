package eu.jpereira.exercises.bets;

import java.math.BigDecimal;

/**
 * A Bet
 */
public class Bet {

    private BetType type;
    private static final int LOWER_BOUND = 1;
    private static final int UPPER_BOUND = 36;

    private final BigDecimal ammount;
    private final String playerName;
    private final String betGoal;
    private final int betNumber;

    public Bet(String thePlayerName, String theBetGoal, BigDecimal betAmmount) {

        if (thePlayerName == null || thePlayerName.trim().isEmpty()) {
            throw new IllegalArgumentException("Player name cannot be null or empty");

        }
        this.playerName = thePlayerName;
        verifyBetGoalAndSetType(theBetGoal);
        //this code is dependent on last statement. do not change order or will break
        if (this.type.equals(BetType.NUMBER)) {
            this.betNumber = Integer.parseInt(theBetGoal);
        } else {
            this.betNumber = -1; //irrelevant
        }
        //


        this.betGoal = theBetGoal;
        verifyAmmount(betAmmount);

        this.ammount = betAmmount;

    }

    /**
     * Guarantees the ammount is allways greater than zero
     *
     * @param betAmmount
     */
    private void verifyAmmount(BigDecimal betAmmount) {

        BigDecimal zero = new BigDecimal("0.0");
        if (zero.compareTo(betAmmount) != -1) {
            throw new IllegalArgumentException("The bet ammount must be a positive number");
        }


    }

    /**
     * Guarantees that no invalid bet is created in the application
     *
     * @param theBetGoal
     */
    private void verifyBetGoalAndSetType(String theBetGoal) {

        boolean valid = false;
        if (!theBetGoal.equals("EVEN") && !theBetGoal.equals("ODD")) {
            //Try a number
            int betNumber;
            try {
                betNumber = Integer.parseInt(theBetGoal);
                if (betNumber >= LOWER_BOUND && betNumber <= UPPER_BOUND) {
                    valid = true;
                    this.type = BetType.NUMBER;
                }
            } catch (NumberFormatException nfe) {
                //ignore
            }
        } else {
            valid = true;
            this.type = BetType.EVEN_ODD;
        }
        if (!valid) {
            throw new IllegalArgumentException("Invalid bet Goal. must be: 1-36, EVEN or ODD");
        }
    }

    /**
     * The name of the player that placed the bet
     *
     * @return
     */
    public String getPlayerName() {
        return this.playerName;
    }

    /**
     * The bet goal, i.e., a number from 1-36, EVEN, or ODD
     *
     * @return
     */
    public String getBetGoal() {
        return this.betGoal;
    }

    /**
     * Th ammount betted
     *
     * @return
     */
    public BigDecimal getAmmount() {
        return this.ammount;
    }

    public BetType getType() {
        return type;
    }

    public int getBetGoalAsNumber() {
        if (!this.type.equals(BetType.NUMBER)) {
            throw new RuntimeException("The bet has a goal that is not a number");
        }
        return this.betNumber;

    }
}
