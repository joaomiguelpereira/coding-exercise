package eu.jpereira.exercises.bets;

import eu.jpereira.exercises.coupier.BetNotAcceptedException;
import eu.jpereira.exercises.coupier.Coupier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

/**
 * This implementation starts a new thread
 */
public class ConsoleBetCollector implements BetCollector {


    private static final String BET_PROMPT_MESSAGE = "Enter a bet in the format: PlayerName BetGoal amount";
    private static final String INVALID_AMMOUNT_VALUE_MESSAGE = "The provided value for amount is not a valid number!";
    private static final String BET_INSTRUCTION_SEPARATOR = " ";
    //the coupier for which this collector is collecting bets
    private final Coupier coupier;

    public ConsoleBetCollector(Coupier theCoupier) {
        if (theCoupier == null) {
            throw new IllegalArgumentException("Coupier can't be null");
        }
        this.coupier = theCoupier;
    }

    @Override
    public void starCollectingBets() {

        if (System.in == null) {
            throw new RuntimeException("This application requires a console to enter data");
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter bet. enter exit or press ctrl+c to exit:");
        try {

            //Control with ctrl+c
            while (true) {
                String betLine = br.readLine();
                if (betLine != null && betLine.equals("exit")) {
                    System.out.println("Exiting...");
                    break;
                }
                Bet bet = readBet(betLine);
                if (bet != null) {
                    try {
                        coupier.acceptBet(bet);
                        System.out.println("Bet Accepted. Add more");
                    } catch (BetNotAcceptedException betNotAcceptedException) {
                        System.err.println(betNotAcceptedException.getMessage());
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * The input are thres strings. First string is player's name, second is Bet goal and third is amount
     *
     * @param betLine
     * @return
     */
    private Bet readBet(String betLine) {
        Bet bet = null;
        if (betLine == null) {
            //program is exiting
            return null;
        }

        String[] tokens = betLine.split(BET_INSTRUCTION_SEPARATOR);

        if (tokens.length != 3) {
            System.err.println(BET_PROMPT_MESSAGE);
            return bet;
        }
        try {
            bet = new Bet(tokens[0].trim(), tokens[1].trim(), new BigDecimal(tokens[2]));
        } catch (NumberFormatException nfe) {
            System.err.println(INVALID_AMMOUNT_VALUE_MESSAGE);
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }
        return bet;


    }
}
