package eu.jpereira.exercises.scores;

import eu.jpereira.exercises.bets.BetOutcome;
import eu.jpereira.exercises.bets.BetResults;
import eu.jpereira.exercises.view.TableRow;
import eu.jpereira.exercises.view.TabularView;

import java.util.Map;

/**
 * A Simple implementation of a Score Book
 */
public class InMemoryScoreBoard implements ScoreBoard {

    //For each player, maintains it's scores
    private final Map<String, ScoreEntry> scores;

    public InMemoryScoreBoard(Map<String, ScoreEntry> scoreEntries) {
        this.scores = scoreEntries;

    }

    @Override
    public boolean hasPlayer(String playerName) {
        return scores.containsKey(playerName);
    }

    @Override
    public void update(BetResults results) {

        for (BetOutcome outcome: results.getBetOutComes() ) {
            ScoreEntry scoreEntry = scores.get(outcome.getPlayerName());
            if (scoreEntry==null) {
                throw new RuntimeException("A player was requested to update a ScoreEntry but the payer does not exists. It should only accept player that are registered");
            }
            scoreEntry.update(outcome.getBetAmmount(), outcome.getwinAmmount());
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        TabularView tabularView = new TabularView();
        tabularView.addHeaders("Player", "Total Win", "Total Bet");
        for (String key : this.scores.keySet() ) {
            ScoreEntry scoreEntry = this.scores.get(key);
            TableRow row = new TableRow();
            row.addCell(0, key);
            row.addCell(1, scoreEntry.getTotalWin().toPlainString());
            row.addCell(2, scoreEntry.getTotalBet().toPlainString());
            tabularView.addRow(row);
        }
        sb.append(tabularView.toString());
        return sb.toString();



    }
}
