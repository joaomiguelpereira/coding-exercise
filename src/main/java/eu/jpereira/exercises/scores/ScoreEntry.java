package eu.jpereira.exercises.scores;

import java.math.BigDecimal;

/**
 * A Score Board entry
 */
public class ScoreEntry {
    private BigDecimal totalWin;
    private BigDecimal totalBet;

    public ScoreEntry(BigDecimal totalWin, BigDecimal totalBet) {

        if (totalWin == null || totalBet == null) {
            throw new IllegalArgumentException("Total win and total bet must not be null");
        }
        this.totalWin = totalWin;
        this.totalBet = totalBet;
    }

    public void update(BigDecimal betAmmount, BigDecimal winnings) {
        this.totalWin = this.totalWin.add(winnings);
        this.totalBet = this.totalBet.add(betAmmount);
    }

    public BigDecimal getTotalWin() {
        return totalWin;
    }

    public BigDecimal getTotalBet() {
        return totalBet;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        ScoreEntry entry = (ScoreEntry) other;

        if (!totalBet.equals(entry.totalBet)) {
            return false;
        }
        if (!totalWin.equals(entry.totalWin)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = totalWin.hashCode();
        result = 31 * result + totalBet.hashCode();
        return result;
    }

}
