package eu.jpereira.exercises.scores;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/15/14
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ScoreBookFileParser {

    Map<String,ScoreEntry> parse() throws ScoreBookFileParserException;
}
