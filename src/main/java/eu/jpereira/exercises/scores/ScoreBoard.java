package eu.jpereira.exercises.scores;

import eu.jpereira.exercises.bets.BetResults;

/**
 * Keep scores
 */
public interface ScoreBoard {
    /**
     * Queries the ScoreBoard if it has a given player
     * @param playerName
     * @return
     */
    boolean hasPlayer(String playerName);

    /**
     * Update the Scores Book with the gioven bet results
     * @param results
     */
    void update(BetResults results);
}
