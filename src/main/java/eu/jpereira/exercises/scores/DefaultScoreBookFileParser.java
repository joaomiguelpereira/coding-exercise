package eu.jpereira.exercises.scores;


import java.io.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Read files where each line contains the following format:
 * PLAYER_NAME,TOTAL_WIN,TOTAL_BET
 * <p/>
 * The PLAYER_NAME is mandatory and the TOTAL_WIN and TOTAL_BET values will default to zero if none provided
 */
public class DefaultScoreBookFileParser implements ScoreBookFileParser {


    private final File file;

    public DefaultScoreBookFileParser(File file) {
        if (file == null || !file.exists() || file.isDirectory() || !file.canWrite() || !file.canRead()) {
            throw new IllegalArgumentException("The file cannot be used. It is either null, does not exists, is a directory or can't be read or write");
        }
        this.file = file;

    }


    @Override
    public Map<String, ScoreEntry> parse() throws ScoreBookFileParserException {

        Map<String, ScoreEntry> result = new HashMap();
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));

            while (in.ready()) {

                String line = in.readLine();
                handleLine(line, result);

            }

            in.close();
        } catch (FileNotFoundException e) {
            throw new ScoreBookFileParserException(e);
        } catch (IOException e) {
            throw new ScoreBookFileParserException(e);
        }
        return result;


    }

    private void handleLine(String line, Map<String, ScoreEntry> result) throws ScoreBookFileParserException {
        if (line.trim().isEmpty()) {
            //Ignore empty lines
            return;
        }
        String[] tokens = line.split(",");

        BigDecimal totalWin = new BigDecimal("0.0");
        BigDecimal totalBet = new BigDecimal("0.0");


        //Trim names
        String playerName = tokens[0].trim();
        if (playerName.isEmpty()) {
            throw new ScoreBookFileParserException("The player name cannot be empty string");
        }
        if (result.containsKey(playerName)) {
            throw new ScoreBookFileParserException(String.format("The player name %s was found in the file more than one time", playerName));
        }

        if (tokens.length >= 2) {
            String totalWinString = tokens[1].trim();
            if (!totalWinString.isEmpty()) {
                try {
                    totalWin = new BigDecimal(totalWinString);
                } catch (NumberFormatException nfe) {
                    throw new ScoreBookFileParserException(String.format("The player %s was found with an invalid value for TOTAL WIN: %s", playerName, totalWinString));
                }
            }


        }
        if (tokens.length >= 3) {
            String totalBetString = tokens[2].trim();
            if (!totalBetString.isEmpty()) {
                try {
                    totalBet = new BigDecimal(totalBetString);
                } catch (NumberFormatException nfe) {
                    throw new ScoreBookFileParserException(String.format("The player %s was found with an invalid value for TOTAL BET: %s", playerName, totalBetString));
                }
            }


        }

        ScoreEntry entry = new ScoreEntry(totalWin, totalBet);
        result.put(playerName, entry);


    }
}
