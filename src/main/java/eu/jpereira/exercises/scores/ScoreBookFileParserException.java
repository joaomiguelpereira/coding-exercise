package eu.jpereira.exercises.scores;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/15/14
 * Time: 11:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScoreBookFileParserException extends Exception{
    public ScoreBookFileParserException(Exception e) {
    }

    public ScoreBookFileParserException(String message) {
        super(message);
    }
}
