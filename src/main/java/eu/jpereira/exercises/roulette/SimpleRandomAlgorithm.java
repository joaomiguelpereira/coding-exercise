package eu.jpereira.exercises.roulette;

import java.util.Random;

/**
 * A simple implementation that uses {@code java.util.Random} for generating a number between 1 and 36
 */
public class SimpleRandomAlgorithm implements RouletteAlgorithm {

    private final Random random;
    private final int lowerBound;
    private final int upperBound;

    public SimpleRandomAlgorithm(int lowerBound, int upperBound) {
        if ( lowerBound == upperBound || lowerBound > upperBound ) {
            throw new IllegalArgumentException("The upper bound must not be equals or less that lower bound");
        }
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.random = new Random();
    }

    @Override
    public int generateResult() {
        //Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value (exclusive),
        return random.nextInt((upperBound-lowerBound)+1)+lowerBound;
    }
}
