package eu.jpereira.exercises.roulette;

/**
 * The RouletteSpinner is responsible to spin a roulette.
 * The RouletteSpinner will only spin the roulette if the start operation has been called before and no call to stop operation has been done meanwhile
 */
public interface RouletteSpinner {

    /**
     * Start the roulette Spinner function
     */
    void start();

    /**
     * Stop the roulette Spinner function
     */
    void stop();

}
