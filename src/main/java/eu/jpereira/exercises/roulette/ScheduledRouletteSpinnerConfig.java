package eu.jpereira.exercises.roulette;

import java.math.BigDecimal;

/**
 * Configuration of a Scheduled Roulette Spinner
 */
public class ScheduledRouletteSpinnerConfig {

    private final int intervalSeconds;
    private final Roulette roulette;

    private ScheduledRouletteSpinnerConfig(int theIntervalSeconds, Roulette theRoulette) {
        this.intervalSeconds = theIntervalSeconds;
        this.roulette = theRoulette;
    }

    public int getIntervalSeconds() {
        return intervalSeconds;
    }

    public Roulette getRoulette() {
        return roulette;
    }

    /**
     * Simple builder for easier configuration
     */
    public static class Builder {
        private int intervalSeconds;
        private Roulette roulette;

        public Builder withFixedIntervalSeconds(int intervalSeconds) {
            this.intervalSeconds = intervalSeconds;
            return this;
        }

        public ScheduledRouletteSpinnerConfig build() {
            if (intervalSeconds <= 0) {
                throw new IllegalArgumentException("Interval must be a positive non zero number");
            }

            if (this.roulette == null) {
                throw new IllegalArgumentException("Roulette cannot be null");
            }
            return new ScheduledRouletteSpinnerConfig(this.intervalSeconds, this.roulette);
        }

        public Builder withRoulette(Roulette theRoulette) {
            this.roulette = theRoulette;
            return this;
        }
    }

}
