package eu.jpereira.exercises.roulette;

/**
 * Spin a roulette
 */
public interface Roulette {
    /**
     * Spin the Roulette
     */
    void spin();

    /**
     * Attache a new observer in this roulette
     * @param observer
     */
    void attachObserver(RouletteObserver observer);
}
