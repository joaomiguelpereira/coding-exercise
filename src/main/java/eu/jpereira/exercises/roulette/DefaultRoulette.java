package eu.jpereira.exercises.roulette;

import java.util.ArrayList;
import java.util.List;

/**
 * Not thread safe
 */
public class DefaultRoulette implements Roulette {

    private final RouletteAlgorithm rouletteAlgorithm;

    private List<RouletteObserver> observers;

    public DefaultRoulette(RouletteAlgorithm rouletteAlgorithm) {
        if (rouletteAlgorithm == null) {
            throw new IllegalArgumentException("Roulette Algorithm cannot be null");
        }
        this.rouletteAlgorithm = rouletteAlgorithm;
        this.observers = new ArrayList();

    }

    @Override
    public void spin() {
        int result = this.rouletteAlgorithm.generateResult();
        notifyObservers(result);


    }

    private void notifyObservers(int result) {
        for (RouletteObserver observer : observers ) {
            observer.resultChanged(result);
        }

    }

    @Override
    public void attachObserver(RouletteObserver observer) {
        if (!this.observers.contains(observer)) {
            observers.add(observer);
        }
    }
}
