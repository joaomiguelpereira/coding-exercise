package eu.jpereira.exercises.roulette;

/**
 * Observes a roulette result
 */
public interface RouletteObserver {

    /**
     * Called with a new result from the roulette
     * @param result
     */
    void resultChanged(int result);
}
