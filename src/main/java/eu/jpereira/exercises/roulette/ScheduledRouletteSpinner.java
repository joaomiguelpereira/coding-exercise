package eu.jpereira.exercises.roulette;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * A Spinner that will spin the roulette in the specified interval.
 **/
public class ScheduledRouletteSpinner implements RouletteSpinner {

    private final Roulette roulette;
    private final int intervalSeconds;
    private ScheduledThreadPoolExecutor executor;

    public ScheduledRouletteSpinner(ScheduledRouletteSpinnerConfig scheduledRouletteSpinnerConfig) {
        this.roulette= scheduledRouletteSpinnerConfig.getRoulette();
        this.intervalSeconds = scheduledRouletteSpinnerConfig.getIntervalSeconds();
        //Just need one thread to run this scheduled task
        this.executor = new ScheduledThreadPoolExecutor(1);
    }

    @Override
    public void start() {
        //Will delay the interval seconds
        this.executor.scheduleAtFixedRate(new ScheduledTask(this.roulette), intervalSeconds, intervalSeconds, TimeUnit.SECONDS);
    }

    @Override
    public void stop() {
        this.executor.shutdown();
    }

    private static class ScheduledTask implements Runnable {

        private final Roulette roulette;

        public ScheduledTask(final Roulette theRoulette) {
            this.roulette = theRoulette;
        }

        @Override
        public void run() {
            roulette.spin();
        }
    }
}
