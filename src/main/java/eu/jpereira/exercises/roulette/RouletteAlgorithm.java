package eu.jpereira.exercises.roulette;

/**
 * An abstraction of a roulette algorithm. It produces a result when request
 */
public interface RouletteAlgorithm {

    int generateResult();
}
