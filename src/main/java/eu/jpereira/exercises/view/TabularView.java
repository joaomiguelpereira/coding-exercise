package eu.jpereira.exercises.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Simple tabular view helper classes. NOT TESTED
 */
public class TabularView {

    private List<String> headers = new ArrayList();
    private List<TableRow> rows = new ArrayList();
    //Will use the indexes of the arrays to match the columns.
    //For each column, keep the greatest string length
    private List<Integer> maxColumnWidths = new ArrayList<Integer>();


    /**
     * DO NOT CALL MORE THAN ONCE. NOT TESTED
     *
     * @param headers
     */
    public void addHeaders(String... headers) {

        this.headers.addAll(Arrays.asList(headers));
        //Calculate widths
        for (int i = 0; i < this.headers.size(); i++) {
            int length = this.headers.get(i).length();
            this.maxColumnWidths.add(i, length);
        }

    }

    public void addRow(TableRow row) {

        this.rows.add(row);
        List<String> cells = row.getCells();

        for (int i = 0; i < cells.size(); i++) {
            int currentValue = maxColumnWidths.get(i);
            int cellLength = cells.get(i).length();
            if (cellLength > currentValue) {
                this.maxColumnWidths.add(i, cellLength);
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int headerIndex = 0;
        for ( String header : headers ) {
            stringBuilder.append(header);
            stringBuilder.append(calculatePadding(headerIndex, header));
            headerIndex++;
        }
        stringBuilder.append("\n--------\n");

        for (TableRow row: rows) {
            int columnIndex = 0;
            for (String cell: row.getCells()) {
                stringBuilder.append(cell);
                stringBuilder.append(calculatePadding(columnIndex, cell));
                columnIndex++;
            }
            stringBuilder.append("\n");
        }


        stringBuilder.append("\n");
        return stringBuilder.toString();

    }

    private String calculatePadding(int headerIndex, String value) {


        //Always add at least three space padding right
        StringBuilder stringBuilder = new StringBuilder("   ");
        int max = this.maxColumnWidths.get(headerIndex);
        int size = max-value.length();


        if (size>0) {

            for (int i=0; i<size;i++) {
                stringBuilder.append(" ");
            }

        }


        return stringBuilder.toString();
    }
}
