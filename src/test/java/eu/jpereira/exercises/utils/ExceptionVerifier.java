package eu.jpereira.exercises.utils;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 12:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class ExceptionVerifier {
    public static void verifyIsThrow(Class scoreBookFileParserExceptionClass, String expectedMessage, Exerciser exerciser) {

        Exception exception = null;
        try {
            exerciser.exercise();
        } catch (Exception e) {
            exception = e;
        }
        assertThat(exception).isNotNull().isInstanceOf(scoreBookFileParserExceptionClass).hasMessage(expectedMessage);

    }

    public interface Exerciser {

        void exercise() throws Exception;
    }
}
