package eu.jpereira.exercises.coupier;

import eu.jpereira.exercises.bets.CurrentBetsBook;
import eu.jpereira.exercises.roulette.Roulette;
import eu.jpereira.exercises.roulette.RouletteObserver;
import eu.jpereira.exercises.roulette.RouletteSpinner;
import eu.jpereira.exercises.scores.ScoreBoard;
import org.junit.Test;


import static org.mockito.Mockito.*;
/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class DefaultCoupierTest {

    @Test
    public void itSouldAttachObserverInTheRoulette() {
        Roulette roulette = mock(Roulette.class);
        RouletteSpinner rouletteSpinner = mock(RouletteSpinner.class);
        CurrentBetsBook currentBetsBook = mock(CurrentBetsBook.class);
        ResultsCalculator resultsCalculator = mock(ResultsCalculator.class);

        ScoreBoard scoreBoard = mock(ScoreBoard.class);

        Coupier coupier = new DefaultCoupier(currentBetsBook, scoreBoard, roulette, rouletteSpinner, resultsCalculator);

        coupier.startAcceptingBets();

        verify(roulette, only()).attachObserver(any(RouletteObserver.class));
    }

}
