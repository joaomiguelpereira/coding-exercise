package eu.jpereira.exercises.coupier;

import eu.jpereira.exercises.bets.Bet;
import eu.jpereira.exercises.bets.BetResults;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import static org.fest.assertions.Assertions.*;
/**
 * Test the default algorithm to calculate results
 */
public class DefaultResultsCalculatorTest {

    @Test
    public void itSHouldCalculateIfBetIsEvenAndResultIsOdd() {
        ResultsCalculator calculator = new DefaultResultsCalculator();
        Bet bet = new Bet("Player", "EVEN", new BigDecimal(10));
        List<Bet> bets = new ArrayList<Bet>();
        bets.add(bet);
        BetResults results = calculator.calculate(bets, 11);
        assertThat(results.getBetOutComes().get(0).getOutcome()).isEqualTo("LOSE");
        assertThat(results.getBetOutComes().get(0).getwinAmmount()).isEqualTo(new BigDecimal("0.0"));
    }

    @Test
    public void itSHouldCalculateIfBetIsEvenAndResultIsEven() {
        ResultsCalculator calculator = new DefaultResultsCalculator();
        Bet bet = new Bet("Player", "EVEN", new BigDecimal("10.0"));
        List<Bet> bets = new ArrayList<Bet>();
        bets.add(bet);
        BetResults results = calculator.calculate(bets, 12);
        assertThat(results.getBetOutComes().get(0).getOutcome()).isEqualTo("WIN");
        assertThat(results.getBetOutComes().get(0).getwinAmmount()).isEqualTo(new BigDecimal("20.0"));
    }

    @Test
    public void itSHouldCalculateIfBetIsOddAndResultIsEven() {
        ResultsCalculator calculator = new DefaultResultsCalculator();
        Bet bet = new Bet("Player", "ODD", new BigDecimal(10));
        List<Bet> bets = new ArrayList<Bet>();
        bets.add(bet);
        BetResults results = calculator.calculate(bets, 12);
        assertThat(results.getBetOutComes().get(0).getOutcome()).isEqualTo("LOSE");
        assertThat(results.getBetOutComes().get(0).getwinAmmount()).isEqualTo(new BigDecimal("0.0"));
    }

    @Test
    public void itSHouldCalculateIfBetIsOddAndResultIsOdd() {
        ResultsCalculator calculator = new DefaultResultsCalculator();
        Bet bet = new Bet("Player", "ODD", new BigDecimal("40.0"));
        List<Bet> bets = new ArrayList<Bet>();
        bets.add(bet);
        BetResults results = calculator.calculate(bets, 11);
        assertThat(results.getBetOutComes().get(0).getOutcome()).isEqualTo("WIN");
        assertThat(results.getBetOutComes().get(0).getwinAmmount()).isEqualTo(new BigDecimal("80.0"));
    }

    @Test
    public void itSHouldCalculateIfBetIsNumberAndResultIsDifferent() {
        ResultsCalculator calculator = new DefaultResultsCalculator();
        Bet bet = new Bet("Player", "36", new BigDecimal("40.0"));
        List<Bet> bets = new ArrayList<Bet>();
        bets.add(bet);
        BetResults results = calculator.calculate(bets, 11);
        assertThat(results.getBetOutComes().get(0).getOutcome()).isEqualTo("LOSE");
        assertThat(results.getBetOutComes().get(0).getwinAmmount()).isEqualTo(new BigDecimal("00.0"));
    }
    @Test
    public void itSHouldCalculateIfBetIsNumberAndResultIsNumber() {
        ResultsCalculator calculator = new DefaultResultsCalculator();
        Bet bet = new Bet("Player", "36", new BigDecimal("40.0"));
        List<Bet> bets = new ArrayList<Bet>();
        bets.add(bet);
        BetResults results = calculator.calculate(bets, 36);
        assertThat(results.getBetOutComes().get(0).getOutcome()).isEqualTo("WIN");
        assertThat(results.getBetOutComes().get(0).getwinAmmount()).isEqualTo(new BigDecimal("1440.0"));
    }


}
