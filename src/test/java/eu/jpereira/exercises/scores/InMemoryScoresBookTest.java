package eu.jpereira.exercises.scores;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.*;
/**
 * Test for InMemoryScoreBoard
 */
public class InMemoryScoresBookTest {


    private static final String PLAYER_NAME = "Player";

    @Test
    public void itSHouldRespondTrueIfQueriedForAnExistingPlayer() {
        Map<String, ScoreEntry> entryMap = new HashMap();
        //win 10, bet 20
        ScoreEntry entry = new ScoreEntry(new BigDecimal("10.0"), new BigDecimal("20.0"));
        entryMap.put(PLAYER_NAME, entry);
        ScoreBoard scoreBoard = new InMemoryScoreBoard(entryMap);

        assertThat(scoreBoard.hasPlayer(PLAYER_NAME)).isTrue();

    }
}
