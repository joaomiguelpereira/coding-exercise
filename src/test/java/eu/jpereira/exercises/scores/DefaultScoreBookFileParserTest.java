package eu.jpereira.exercises.scores;

import eu.jpereira.exercises.utils.ExceptionVerifier;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.Map;

import static org.fest.assertions.Assertions.*;
import static org.fest.assertions.MapAssert.*;


/**
 * Test parsing of input file
 */
public class DefaultScoreBookFileParserTest {


    @Test
    public void itShouldThrowExceptionIfFileDoesNotExists() throws ScoreBookFileParserException {

        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class, "The file cannot be used. It is either null, does not exists, is a directory or can't be read or write", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                File file = new File("nofile");
                ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);
                parser.parse();
            }
        });
    }


    @Test
    public void itShouldThrowExceptionIfRepeatedNamesAreFound() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("duplicate_players_names.txt").getFile());
        final ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        ExceptionVerifier.verifyIsThrow(ScoreBookFileParserException.class, "The player name Barbara was found in the file more than one time", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                parser.parse();
            }
        });
    }


    @Test
    public void itShouldThrowExceptionIfNameIsEmpty() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("empty_players_names.txt").getFile());
        final ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        ExceptionVerifier.verifyIsThrow(ScoreBookFileParserException.class, "The player name cannot be empty string", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                parser.parse();
            }
        });
    }

    @Test
    public void itShouldThrowExceptionValueForTotalWinIsInvalid() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("wrong_total_win.txt").getFile());
        final ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        ExceptionVerifier.verifyIsThrow(ScoreBookFileParserException.class, "The player Tiki_Monkey was found with an invalid value for TOTAL WIN: ee", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                parser.parse();
            }
        });
    }


    @Test
    public void itShouldThrowExceptionValueForTotalBetIsInvalid() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("wrong_total_bet.txt").getFile());
        final ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        ExceptionVerifier.verifyIsThrow(ScoreBookFileParserException.class, "The player Tiki_Monkey was found with an invalid value for TOTAL BET: 1.A", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                parser.parse();
            }
        });
    }


    @Test
    public void itShouldIgnoreEmptyLines() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("only_players_names_with_empty_lines.txt").getFile());
        final ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        Map<String, ScoreEntry> results = parser.parse();
        assertThat(results).isNotNull();
        assertThat(results).includes(entry("Tiki_Monkey", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));
        assertThat(results).includes(entry("Barbara", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));

        assertThat(results).includes(entry("Ambrosia", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));
        assertThat(results).includes(entry("Joana", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));
    }


    @Test
    public void itSHouldParseIfOnlyNamesAreProvided() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("only_players_names.txt").getFile());
        ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        Map<String, ScoreEntry> results = parser.parse();
        assertThat(results).isNotNull();
        assertThat(results).includes(entry("Tiki_Monkey", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));
        assertThat(results).includes(entry("Barbara", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));

    }

    @Test
    public void itSHouldParseIfOnlySomeDataIsProvided() throws ScoreBookFileParserException {
        File file = new File(getClass().getClassLoader().getResource("partial_data_file.txt").getFile());
        ScoreBookFileParser parser = new DefaultScoreBookFileParser(file);

        Map<String, ScoreEntry> results = parser.parse();
        assertThat(results).isNotNull();
        assertThat(results).includes(entry("Tiki_Monkey", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("0.0"))));
        assertThat(results).includes(entry("Barbara", new ScoreEntry(new BigDecimal("2.2"), new BigDecimal("3.3"))));

        assertThat(results).includes(entry("Ambrosia", new ScoreEntry(new BigDecimal("0.0"), new BigDecimal("5.4"))));
        assertThat(results).includes(entry("Joana", new ScoreEntry(new BigDecimal("4.0"), new BigDecimal("0.0"))));

    }
}
