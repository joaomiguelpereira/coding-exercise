package eu.jpereira.exercises.bets;

import eu.jpereira.exercises.utils.ExceptionVerifier;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class BetTest {

    @Test
    public void itShouldHaveNonNullPlayerName() {
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class,"Player name cannot be null or empty", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                    new Bet(null, "1", new BigDecimal("1.0"));

            }
        });

    }

    @Test
    public void itShouldHaveNonEmptyPlayerName() {
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class,"Player name cannot be null or empty", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                new Bet(" ", "1", new BigDecimal("1.0"));

            }
        });

    }

    @Test
    public void itShouldHaveValidBetGoalAsNumberPlayerName() {
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class,"Invalid bet Goal. must be: 1-36, EVEN or ODD", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                new Bet("player", "0", new BigDecimal("1.0"));

            }
        });

        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class,"Invalid bet Goal. must be: 1-36, EVEN or ODD", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                new Bet("player", "47", new BigDecimal("1.0"));

            }
        });


    }

    @Test
    public void itShouldHavePositiveValueForBetAmmount() {
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class,"The bet ammount must be a positive number", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                new Bet("player", "1", new BigDecimal("0.0"));

            }
        });

        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class,"The bet ammount must be a positive number", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                new Bet("player", "1", new BigDecimal("-0.1"));

            }
        });
    }

    @Test
    public void itShouldCreateValidBets() {
        new Bet("jonh","EVEN",new BigDecimal("9.2"));
        new Bet("jonh","ODD",new BigDecimal("0.1"));
        new Bet("jonh","1",new BigDecimal("0.01"));
        new Bet("jonh","36",new BigDecimal("0.01"));
        new Bet("jonh","20",new BigDecimal("1.01"));
    }
}
