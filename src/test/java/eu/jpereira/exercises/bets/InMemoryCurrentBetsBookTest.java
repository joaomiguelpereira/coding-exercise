package eu.jpereira.exercises.bets;

import eu.jpereira.exercises.utils.ExceptionVerifier;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.*;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class InMemoryCurrentBetsBookTest {

    @Test
    public void itShouldThrowExceptionIfBetIsNull() {

        final CurrentBetsBook currentBetsBook = new InMemoryCurrentBetsBook();

        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class, "Bet cannot be null", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                currentBetsBook.placeBet(null);
            }
        });
    }

    @Test
    public void itShouldPlaceBets() {
        final CurrentBetsBook currentBetsBook = new InMemoryCurrentBetsBook();
        currentBetsBook.placeBet(new Bet("Jonh", "EVEN", new BigDecimal(10)));
        currentBetsBook.placeBet(new Bet("Jonh", "EVEN", new BigDecimal(20)));
        currentBetsBook.placeBet(new Bet("Jonh", "ODD", new BigDecimal(30)));
        currentBetsBook.placeBet(new Bet("Jonh", "33", new BigDecimal(40)));
        assertThat(currentBetsBook.getBets()).hasSize(4);
    }

    @Test
    public void itShouldResetBook() {
        final CurrentBetsBook currentBetsBook = new InMemoryCurrentBetsBook();
        currentBetsBook.placeBet(new Bet("Jonh", "EVEN", new BigDecimal(10)));
        currentBetsBook.placeBet(new Bet("Jonh", "EVEN", new BigDecimal(20)));
        currentBetsBook.reset();
        assertThat(currentBetsBook.getBets()).isEmpty();
    }



}
