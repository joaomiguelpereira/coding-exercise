package eu.jpereira.exercises.roulette;

import org.junit.Ignore;
import org.junit.Test;

import static org.fest.assertions.Assertions.*;

/**
 * Test ScheduledRouletteSpinner
 */
public class ScheduledRouletteSpinnerTest {


    private static final long TEN_SECONDS = 10000;
    private static final long TWO_SECONDS = 2000;

    @Test
    @Ignore("This is a test that takes too long so is ignored by default. Aditionally it is undeterministic")
    public void itRunsEveryThirtySeconds() {


        FakeRoulette fakeRoulette = new FakeRoulette();
        ScheduledRouletteSpinnerConfig config = new ScheduledRouletteSpinnerConfig.Builder().
                withRoulette(fakeRoulette).
                withFixedIntervalSeconds(1).build();


        RouletteSpinner spinner = new ScheduledRouletteSpinner(config);

        long startTime = System.currentTimeMillis();
        boolean done = false;
        spinner.start();
        while(!done) {
            long currentTime = System.currentTimeMillis();
            if ((currentTime-startTime)>= TEN_SECONDS) {
                done = true;
            }

        }
        spinner.stop();

        //This is not an deterministic behaviour
        assertThat(fakeRoulette.calls).isGreaterThanOrEqualTo(10);


    }

    @Test
    public void itShouldSpinTheRoulette() {


        FakeRoulette fakeRoulette = new FakeRoulette();
        ScheduledRouletteSpinnerConfig config = new ScheduledRouletteSpinnerConfig.Builder().
                withRoulette(fakeRoulette).
                withFixedIntervalSeconds(1).build();


        RouletteSpinner spinner = new ScheduledRouletteSpinner(config);

        long startTime = System.currentTimeMillis();
        boolean done = false;
        spinner.start();

        while(!done) {
            long currentTime = System.currentTimeMillis();
            if ((currentTime-startTime)>= TWO_SECONDS) {
                done = true;
            }

        }
        spinner.stop();

        assertThat(fakeRoulette.calls).isGreaterThanOrEqualTo(1);


    }


    private class FakeRoulette implements Roulette {
        int calls = 0;
        @Override
        public void spin() {
            calls++;

        }

        @Override
        public void attachObserver(RouletteObserver observer) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}
