package eu.jpereira.exercises.roulette;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Test for the Algorithm
 */
public class SimpleRandomAlgorithmTest {

    private static int FIVE_SECONDS = 5000;
    private static int MIN = 0;
    private static int MAX = 36;


    @Test
    /**
     * This is testing if all the possible results at least get one hit when the algorithm runs several times.
     * Since the implementation is backed by {@code java.util.Random}, no tests are need
     */
    public void itSHouldProduceAtLeastOneTimeForEachExpetctedNumber() {

        Map<Integer, Integer> hitsPerNumber = new HashMap();
        for (int i=MIN; i<=MAX; i++) {
            hitsPerNumber.put(i, 0);
        }
        RouletteAlgorithm algorithm = new SimpleRandomAlgorithm(MIN, MAX);

        long startTime = System.currentTimeMillis();
        long currentTime;
        boolean done = false;
        while (!done ) {

            int result = algorithm.generateResult();

            int count = hitsPerNumber.get(result);
            hitsPerNumber.put(result, count+1);


            currentTime = System.currentTimeMillis();
            if ( (currentTime - startTime) >  FIVE_SECONDS) {
                done = true;
            }
        }
        System.out.println(hitsPerNumber);
        for (Integer count : hitsPerNumber.values() ) {
            assertThat(count).isNotEqualTo(0);
        }



    }

    @Test
    public void itShouldProduceANumberBetweenOneAndThirtySix() {

        RouletteAlgorithm algorithm = new SimpleRandomAlgorithm(MIN, MAX);

        //The idea is to run the algorithm a few times and check if it always a number within it's boundaries
        long startTime = System.currentTimeMillis();
        long currentTime;
        boolean done = false;
        while (!done ) {
            assertThat(algorithm.generateResult()).isGreaterThanOrEqualTo(MIN);
            assertThat(algorithm.generateResult()).isLessThanOrEqualTo(MAX);
            currentTime = System.currentTimeMillis();
            if ( (currentTime - startTime) >  FIVE_SECONDS ) {
                done = true;
            }
        }
    }

}
