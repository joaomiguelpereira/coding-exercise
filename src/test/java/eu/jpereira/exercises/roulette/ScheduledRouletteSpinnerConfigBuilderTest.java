package eu.jpereira.exercises.roulette;

import eu.jpereira.exercises.utils.ExceptionVerifier;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.fest.assertions.Assertions.*;

/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 1:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScheduledRouletteSpinnerConfigBuilderTest {

    @Test
    public void itShouldThrowExceptionIfIntervalIsZero() {

        final ScheduledRouletteSpinnerConfig.Builder builder = new ScheduledRouletteSpinnerConfig.Builder();
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class, "Interval must be a positive non zero number", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
               builder.withFixedIntervalSeconds(0).build();

            }
        });
    }


    @Test
    public void itShouldThrowExceptionIfIntervalIsLessThanZero() {

        final ScheduledRouletteSpinnerConfig.Builder builder = new ScheduledRouletteSpinnerConfig.Builder();
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class, "Interval must be a positive non zero number", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                builder.withFixedIntervalSeconds(-1).build();

            }
        });
    }
    @Test
    public void itShouldThrowExceptionIfRouletteIsNull() {

        final ScheduledRouletteSpinnerConfig.Builder builder = new ScheduledRouletteSpinnerConfig.Builder();
        ExceptionVerifier.verifyIsThrow(IllegalArgumentException.class, "Roulette cannot be null", new ExceptionVerifier.Exerciser() {
            @Override
            public void exercise() throws Exception {
                builder.withFixedIntervalSeconds(1).withRoulette(null).build();

            }
        });
    }

    @Test
    public void itShouldBuildValidScheduledRouletteConfig() {

        Roulette mockedRoulette =   mock(Roulette.class);
        ScheduledRouletteSpinnerConfig.Builder builder = new ScheduledRouletteSpinnerConfig.Builder();
        ScheduledRouletteSpinnerConfig config = builder.withFixedIntervalSeconds(30).withRoulette(mockedRoulette).build();

        assertThat(config.getIntervalSeconds()).isEqualTo(30);
        assertThat(config.getRoulette()).isEqualTo(mockedRoulette);



    }
}
