package eu.jpereira.exercises.roulette;

import org.junit.Test;

import static org.mockito.Mockito.*;
/**
 * Created with IntelliJ IDEA.
 * User: jpereira
 * Date: 11/16/14
 * Time: 2:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class DefaultRouletteTest {

    @Test
    public void itShouldProduceAResultWhenSpins() {
        RouletteAlgorithm mockAlgorithm = mock(RouletteAlgorithm.class);
        Roulette roulette = new DefaultRoulette(mockAlgorithm);
        roulette.spin();
        verify(mockAlgorithm).generateResult();
    }

    @Test
    public void itShouldNotifyObservers() {
        RouletteAlgorithm mockAlgorithm = mock(RouletteAlgorithm.class);
        Roulette roulette = new DefaultRoulette(mockAlgorithm);
        RouletteObserver observer = mock(RouletteObserver.class);
        RouletteObserver secondObserver = mock(RouletteObserver.class);
        when(mockAlgorithm.generateResult()).thenReturn(10);

        roulette.attachObserver(observer);
        roulette.attachObserver(secondObserver);

        roulette.spin();
        verify(observer).resultChanged(10);
        verify(secondObserver).resultChanged(10);

    }
}
